﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using NaviconTestConsole.Models;
using Newtonsoft.Json;
using static System.Console;

namespace NaviconTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");

                var configuration = builder.Build();

                var serviceUri = configuration["serviceUri"];
                var htmlUri = configuration["htmlUri"];
                var threadCount = configuration["threadCount"];
                var imageCount = configuration["imageCount"];
                var uri = $"{serviceUri}?url={htmlUri}&threadCount={threadCount}&imageCount={imageCount}";

                Console.WriteLine("Отправка запроса по адресу: " + uri);
                var sw = System.Diagnostics.Stopwatch.StartNew();
                var task = GetImageGroupsAsync(uri);
                task.Wait();
                sw.Stop();
                Console.WriteLine($"Результат получен за: {sw.ElapsedMilliseconds} мс.");
                Console.WriteLine(JsonConvert.SerializeObject(task.Result, Formatting.Indented));
            }
            catch (Exception ex)
            {
                WriteLine(ex.ToString());
            }
            ReadKey();
        }

        private static async Task<List<ImageGroup>> GetImageGroupsAsync(string uri)
        {
            var httpClient = new HttpClient();
            var content = await httpClient.GetStringAsync(uri);
            return await Task.Run(() => JsonConvert.DeserializeObject<List<ImageGroup>>(content));
        }
    }
}
