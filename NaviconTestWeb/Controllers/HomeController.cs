using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NaviconTestWeb.Extensions;
using NaviconTestWeb.Services;
using Newtonsoft.Json;

namespace NaviconTestWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly HtmlParser _htmlParser;
        private readonly ImageLoader _imageLoader;

        public HomeController(ILogger<HomeController> logger, HtmlParser htmlParser, ImageLoader imageLoader)
        {
            _logger = logger;
            _htmlParser = htmlParser;
            _imageLoader = imageLoader;
        }

        public IActionResult Index()
        {
            return Content("Application works.");
        }

        //[Route("GetImages")]
        [HttpGet("/GetImages")]
        public async Task<ActionResult> GetImages(string url, int threadCount, int imageCount)
        {
            try
            {
                _logger.LogInformation($"Параметры: url='{url}', threadCount='{threadCount}', imageCount='{imageCount}'");

                var imageInfos = await _htmlParser.GetImageInfosAsync(url, imageCount);

                _imageLoader.DownloadImages(imageInfos, threadCount);

                var groupedImages = imageInfos.GetImageGroups();

                _logger.LogInformation("Результат отправлен.");
                return Json(groupedImages);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return StatusCode(500, ex.Message);
            }
        }
    }
}