using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;
using NaviconTestWeb.Models;

namespace NaviconTestWeb.Extensions
{
    public static class Extensions
    {
        public static void LogError(this ILogger logger, Exception ex)
        {
            logger.LogError(ex, ex.Message);
        }

        public static string RemoveInvalidFileNameChars(this string filename)
        {
            return string.Concat(filename.Split(Path.GetInvalidFileNameChars()));
        }

        public static List<ImageGroup> GetImageGroups(this IEnumerable<ImageInfo> imageInfos)
        {
            var imageGroups = new List<ImageGroup>();
            foreach (var imageInfo in imageInfos)
            {
                var host = new Uri(imageInfo.src).Host;
                var imageGroup = imageGroups.SingleOrDefault(x => x.host == host);
                if (imageGroup == null)
                {
                    imageGroup = new ImageGroup()
                    {
                        host = host,
                        images = new List<ImageInfo>() { imageInfo }
                    };
                    imageGroups.Add(imageGroup);
                }
                else
                {
                    imageGroup.images.Add(imageInfo);
                }
            }
            return imageGroups;
        }

        public static List<List<T>> SplitExactly<T>(this IList<T> array, int size)
        {
            var splitArray = array.Split(size);
            var result = new List<List<T>>();
            foreach (var item in splitArray)
            {
                result.Add(item.ToList());
            }
            if (result.Count > size)
            {
                var lastArrayIndex = size;
                for (int i = 0; i < result[lastArrayIndex].Count; i++)
                {
                    result[i].Add(result[lastArrayIndex][i]);
                }
                result.RemoveAt(lastArrayIndex);
            }
            return result;
        }

        private static IEnumerable<IEnumerable<T>> Split<T>(this IList<T> array, int size)
        {
            var countRemaining = array.Count;
            var countToTake = array.Count / size;
            for (var i = 0; i < size; i++)
            {
                countRemaining -= countToTake;
                yield return array.Skip(i * countToTake).Take(countToTake);
            }
            if (countRemaining > 0)
            {
                yield return array.Skip(array.Count - countRemaining).Take(countRemaining);
            }
        }
    }
}