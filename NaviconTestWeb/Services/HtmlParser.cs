﻿using Microsoft.Extensions.Logging;
using NaviconTestWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NaviconTestWeb.Services
{
    public class HtmlParser
    {
        private string _host;
        private readonly ILogger<HtmlParser> _logger;

        public HtmlParser(ILogger<HtmlParser> logger)
        {
            _logger = logger;
        }

        public async Task<List<ImageInfo>> GetImageInfosAsync(string uri, int imageCount)
        {
            _host = new Uri(uri).Host;

            var imageInfos = new List<ImageInfo>();
            var htmlString = await GetHtmlString(uri);
            if (!string.IsNullOrWhiteSpace(htmlString))
            {
                var imageTags = GetImageTags(htmlString);
                if (imageTags.Length > 0)
                {
                    imageInfos = GetImageInfos(imageTags);
                }
                imageInfos = imageInfos.Take(imageCount).ToList();
            }
            return imageInfos;
        }

        private List<ImageInfo> GetImageInfos(IEnumerable<string> imageTags)
        {
            var imageInfos = new List<ImageInfo>();
            foreach (var imageTag in imageTags)
            {
                var alt = GetAttribute(imageTag, "alt");
                var src = GetAttribute(imageTag, "src");
                imageInfos.Add(new ImageInfo { alt = alt, src = src });
            }
            CleanImageInfos(imageInfos);
            return imageInfos;
        }

        private void CleanImageInfos(List<ImageInfo> imageInfos)
        {
            for (int i = 0; i < imageInfos.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(imageInfos[i].src))
                {
                    imageInfos.RemoveAt(i);
                    i--;
                }
                if (!imageInfos[i].src.ToLower().StartsWith("http"))
                {
                    imageInfos[i].src = "http://" + _host + "/" + imageInfos[i].src.TrimStart('/');
                }
            }
        }

        private string GetAttribute(string img, string attributeName)
        {
            var regex = new Regex($"{attributeName}=\"(.*?)\"");
            var matches = regex.Matches(img);
            var imageTags = matches
                .Cast<Match>()
                .Select(m => m.Value)
                .ToArray();
            if (imageTags.Length > 0)
            {
                var attributeValue = imageTags[0];
                attributeValue = attributeValue.Remove(0, attributeName.Length + 1);
                attributeValue = attributeValue.Trim('\"');
                return attributeValue;
            }
            return string.Empty;
        }

        private async Task<string> GetHtmlString(string htmlUri)
        {
            using (var client = new WebClient())
            {
                var request = WebRequest.Create(htmlUri);
                _host = request.RequestUri.Host;
                using (var response = request.GetResponse())
                {
                    client.Headers.Add(HttpRequestHeader.ContentType, response.ContentType);
                }
                string htmlString = await client.DownloadStringTaskAsync(new Uri(htmlUri));
                _logger.LogInformation($"Документ {htmlUri} загружен.");
                return htmlString;
            }
        }

        private string[] GetImageTags(string htmlString)
        {
            var regex = new Regex(@"<img[\ \t.]*?[^\>]*>");
            var matches = regex.Matches(htmlString);
            var imageTags = matches
                .Cast<Match>()
                .Select(m => m.Value)
                .ToArray();
            return imageTags;
        }
    }
}
