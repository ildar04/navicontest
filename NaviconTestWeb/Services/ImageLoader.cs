﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NaviconTestWeb.Extensions;
using NaviconTestWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NaviconTestWeb.Services
{
    public class ImageLoader
    {
        private readonly ILogger<ImageLoader> _logger;
        private readonly IConfiguration _configuration;
        private readonly string _imagesFolder;

        public ImageLoader(IConfiguration configuration, ILogger<ImageLoader> logger)
        {
            _logger = logger;
            _configuration = configuration;
            _imagesFolder = configuration["Settings:ImagesFolder"];
        }

        public void DownloadImages(List<ImageInfo> imageInfos, int threadCount)
        {
            try
            {
                if (imageInfos.Count > 0)
                {
                    var useThreadPool = Convert.ToBoolean(_configuration["Settings:useThreadPool"]);
                    var timer = Stopwatch.StartNew();
                    if (useThreadPool)
                        InternalDownloadImagesTasks(imageInfos, threadCount);
                    else
                        InternalDownloadImagesThreads(imageInfos, threadCount);

                    timer.Stop();
                    _logger.LogInformation($"Скачивание файлов завершено успешно за {timer.ElapsedMilliseconds} мс.");
                }
                else
                {
                    _logger.LogInformation("Изображения не найдены");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
            }
        }

        private void InternalDownloadImagesTasks(List<ImageInfo> imageInfos, int threadCount)
        {
            if (threadCount < Environment.ProcessorCount)
                ThreadPool.SetMaxThreads(Environment.ProcessorCount, Environment.ProcessorCount);
            else
                ThreadPool.SetMaxThreads(threadCount, threadCount);

            Directory.CreateDirectory(_imagesFolder);
            Console.WriteLine($"Начало загрузки изображений используя TPL");
            var tasks = new List<Task>();
            foreach (var imageInfo in imageInfos)
            {
                var task = Task.Run(() => DownloadImageAsync(imageInfo));
                tasks.Add(task);
            }
            Task.WaitAll(tasks.ToArray());
        }

        private void InternalDownloadImagesThreads(List<ImageInfo> imageInfos, int threadCount)
        {
            Directory.CreateDirectory(_imagesFolder);

            // Ускоряет загрузку файлов если производятся продолжительные асинхронный ввод/вывод, действия с сетью
            var threadsCountMultiplier = Convert.ToDouble(_configuration["Settings:threadsCountMultiplier"]);

            var realThreadCount = (int)(Environment.ProcessorCount * threadsCountMultiplier);

            if (threadCount < realThreadCount)
            {
                realThreadCount = threadCount;
            }
            if (realThreadCount > imageInfos.Count)
            {
                realThreadCount = imageInfos.Count;
            }

            var threads = new Thread[realThreadCount];
            Console.WriteLine($"InternalDownloadImagesTasks: количество потоков: {threads.Length}");
            var splitedImageInfos = imageInfos.SplitExactly(realThreadCount);
            var i = 0;
            foreach (var splitedImageInfo in splitedImageInfos)
            {
                var timer = Stopwatch.StartNew();
                threads[i] = new Thread(() =>
                {
                    foreach (var imageInfo in splitedImageInfo)
                    {
                        var task = DownloadImageAsync(imageInfo);
                        task.Wait();
                    }
                });
                threads[i].Start();
                timer.Stop();
                _logger.LogInformation($"Поток №{i} запустился за {timer.ElapsedMilliseconds} мс.");
                i++;
            }
            foreach (Thread thread in threads)
            {
                thread.Join();
            }
        }

        private async Task<int> DownloadImageAsync(ImageInfo image)
        {
            var fileName = "";
            long downloadingTime;
            try
            {
                using (var webResponse = await HttpWebRequest.Create(image.src).GetResponseAsync())
                {
                    fileName = GetFileName(webResponse.Headers["content-disposition"], image.src);
                    var fullFilePath = _imagesFolder + Path.DirectorySeparatorChar + fileName;

                    using (var sr = new StreamReader(webResponse.GetResponseStream()))
                    using (var sw = new StreamWriter(fullFilePath))
                    {
                        var timer = Stopwatch.StartNew();
                        const int BUFFER_SIZE = 16 * 1024;
                        var buffer = new char[BUFFER_SIZE];
                        int bytesRead;
                        while ((bytesRead = await sr.ReadAsync(buffer, 0, BUFFER_SIZE)) > 0)
                        {
                            await sw.WriteAsync(buffer, 0, bytesRead);
                        }
                        timer.Stop();
                        downloadingTime = timer.ElapsedMilliseconds;
                        image.size = new FileInfo(fullFilePath).Length;
                    }
                }
                _logger.LogInformation($"Файл загружен: {fileName}\n Время загрузки: {downloadingTime} мс.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,
                    $"Ошибка загрузки файла: {JsonConvert.SerializeObject(image, Formatting.Indented)}");
            }
            return Thread.CurrentThread.ManagedThreadId;
        }

        private string GetFileName(string contentDisposition, string imageSrc)
        {
            var fileName = string.Empty;
            if (!string.IsNullOrEmpty(contentDisposition))
            {
                string lookFor = "filename=";
                int index = contentDisposition.IndexOf(lookFor, StringComparison.CurrentCultureIgnoreCase);
                if (index >= 0)
                    fileName = contentDisposition.Substring(index + lookFor.Length);
            }

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = imageSrc.RemoveInvalidFileNameChars();
            }
            return fileName;
        }
    }
}
