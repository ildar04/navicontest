﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaviconTestWeb.Models
{
    public class ImageGroup
    {
        public string host { get; set; }
        public List<ImageInfo> images { get; set; }
    }
}
