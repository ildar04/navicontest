﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NaviconTestWeb.Models
{
    public class ImageInfo
    {
        public string alt { get; set; }
        public string src { get; set; }
        public long size { get; set; }
    }
}
